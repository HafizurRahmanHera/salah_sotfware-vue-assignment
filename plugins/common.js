import Vue from "vue";
import { mapGetters } from "vuex";

Vue.filter(
  'numberWithCommas', function (value) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    // return 1258697 as 1,258,697
  }
);

Vue.mixin({
  methods: {
    // async callApi(method, url, dataObj) {
    //   try {
    //     let data = await this.$axios({
    //       method: method,
    //       // url: url,
    //       url: 'app/'+url,
    //       data: dataObj
    //     });
    //     return data;
    //   } catch (e) {
    //     return e.response;
    //   }
    // },


    getDiscountAmount(discount,amount){
      return amount-Math.round((amount/100)*discount)
    },

    // Add product to cart
    addToCart(product, quantity=1){
      //check if product already exit in cart
      let found_index = -1
      for (let index = 0; index < this.getCartList.length; index++) {
        if (this.getCartList[index].id==product.id) {
          found_index = index
          break
        }
      }

      // if product exit or else
      if (found_index>=0) {
        // add quatity
        this.$store.dispatch('addCartListItemQuantity', {index:found_index, quantity:quantity})
      }else{
        product.quantity = quantity
        this.$store.dispatch('addCartListItem', product)
      }

      // add quantity price to cart subtotal
      let total_price = product.price*quantity
      this.$store.dispatch('updateCartSubtoal', this.getCartSubtotal+total_price)

      // console.log('this.getCartList: ',this.getCartList)
      // console.log('this.getCartSubtotal: ',this.getCartSubtotal)
    },
    //  Remove product form cart
    removeCartListItem(index,product){
        let price = product.price*product.quantity
        this.$store.dispatch('removeCartListItem', index)
        this.$store.dispatch('updateCartSubtoal', this.getCartSubtotal-price)
    },

    //  Removing all items from cart
    emptyCartList(){
        this.$store.dispatch('emptyCartList',[])
    },


  },
  computed: {
    ...mapGetters({
      authInfo: "getAuthInfo",
      isLoggedIn: "getIsLoggedIn",

      getProducts:"getProducts",
      getCartList:"getCartList",
      getCartSubtotal:"getCartSubtotal",
      getTaxesPercentage:"getTaxesPercentage",
    })
  }
});
