export const strict = false
export const state = () => ({
  authInfo: false,

  products:[
    {
      id:1,
      image_link:'https://www.w3schools.com/bootstrap4/paris.jpg',
      name:'product 1',
      price:120,
      description:'Lorem Ipsum is simply dummy text'
    },
    {
      id:2,
      image_link:'https://cdn.pixabay.com/photo/2020/08/13/14/15/mountains-5485366__340.jpg',
      // image_link:null,
      name:'product 2',
      price:220,
      description:'Lorem Ipsum is simply dummy text'
    },
    {
      id:3,
      image_link:'https://cdn.pixabay.com/photo/2020/05/26/13/22/puffins-5223055__340.jpg',
      name:'product 3',
      price:100,
      description:'Lorem Ipsum is simply dummy text'
    },
    {
      id:4,
      image_link:'https://cdn.pixabay.com/photo/2020/05/11/17/08/boat-5159224__340.jpg',
      name:'product 4',
      price:200,
      description:'Lorem Ipsum is simply dummy text'
    },
    {
      id:5,
      image_link:'https://cdn.pixabay.com/photo/2020/08/14/12/34/saltburn-pier-5487835__340.jpg',
      name:'product 5',
      price:50,
      description:'Lorem Ipsum is simply dummy text'
    },
    // {
    //   id:6,
    //   image_link:'https://cdn.pixabay.com/photo/2020/03/09/14/54/seychelles-4916045__340.jpg',
    //   name:'product 6',
    //   price:90,
    // },
  ],
  cart_list:[
    // {
    //   id:2,
    //   image_link:'https://www.w3schools.com/bootstrap4/paris.jpg',
    //   name:'product 1',
    //   price:120,
    //   quantity:1
    // },
    // {
    //   id:1,
    //   image_link:'https://www.w3schools.com/bootstrap4/paris.jpg',
    //   name:'product 1',
    //   price:120,
    //   quantity:1
    // },
    // {
    //   id:3,
    //   image_link:'https://www.w3schools.com/bootstrap4/paris.jpg',
    //   name:'product 1',
    //   price:120,
    //   quantity:1
    // },
    // {
    //   id:4,
    //   image_link:'https://www.w3schools.com/bootstrap4/paris.jpg',
    //   name:'product 1',
    //   price:120,
    //   quantity:1
    // },
    // {
    //   id:5,
    //   image_link:'https://www.w3schools.com/bootstrap4/paris.jpg',
    //   name:'product 1',
    //   price:120,
    //   quantity:1
    // },
  ],
  cart_subtotal:0,
  taxes_percentage:2,
})

// common getters
export const getters = {
  getIsLoggedIn (state) {
    return !!state.authInfo
  },
  getAuthInfo (state) {
    return state.authInfo
  },

  getProducts (state) {
    return state.products
  },
  getCartList (state) {
    return state.cart_list
  },
  getCartSubtotal (state) {
    return state.cart_subtotal
  },
  getTaxesPercentage (state) {
    return state.taxes_percentage
  },

}

//mutations for changing data from action
export const mutations = {
  setAuthInfo (state, data) {
    // console.log('setAuthInfo')
    state.authInfo = data
  },

  setProducts (state, data) {
    state.products = data
  },
  deleteProductFromIndex (state, data) {
    state.products.splice(data,1)
  },
  updateCartList (state, data) {
    state.cart_list = data
  },
  emptyCartList (state, data) {
    state.cart_list = []
  },
  addCartListItem (state, data) {
    state.cart_list.push(data)
  },
  removeCartListItem (state, index) {
    state.cart_list.splice(index,1)
  },
  updateCartSubtoal (state, data) {
    state.cart_subtotal = data
  },
  addCartSubtoal (state, data) {
    state.cart_subtotal += data
  },
  minusCartSubtoal (state, data) {
    state.cart_subtotal -= data
  },
  addCartListItemQuantity (state, data) {
    state.cart_list[data.index].quantity += data.quantity
  },
  updateCartListItemQuantity (state, data) {
    state.cart_subtotal-= state.cart_list[data.index].price*state.cart_list[data.index].quantity
    state.cart_list[data.index].quantity = data.quantity
    state.cart_subtotal+= state.cart_list[data.index].price*state.cart_list[data.index].quantity
  },

}

// actionns for commiting mutations
export const actions = {
  async nuxtServerInit ({ commit }, {$axios}) {
    try {
      // get the initial data

    } catch (e) {
      // console.log(e.response)
    }
  },
  setAuthInfo ({commit}, data) {
    commit('setAuthInfo', data)
  },

  setProducts ({commit}, data) {
    commit('setProducts', data)
  },
  deleteProductFromIndex ({commit}, data) {
    commit('deleteProductFromIndex', data)
  },
  updateCartList ({commit}, data) {
    commit('updateCartList', data)
  },
  emptyCartList ({commit}, data) {
    commit('emptyCartList', data)
  },
  addCartListItem ({commit}, data) {
    commit('addCartListItem', data)
  },
  removeCartListItem ({commit}, data) {
    commit('removeCartListItem', data)
  },
  updateCartSubtoal ({commit}, data) {
    commit('updateCartSubtoal', data)
  },
  addCartSubtoal ({commit}, data) {
    commit('addCartSubtoal', data)
  },
  minusCartSubtoal ({commit}, data) {
    commit('minusCartSubtoal', data)
  },
  addCartListItemQuantity ({commit}, data) {
    commit('addCartListItemQuantity', data)
  },
  updateCartListItemQuantity ({commit}, data) {
    commit('updateCartListItemQuantity', data)
  },

}

